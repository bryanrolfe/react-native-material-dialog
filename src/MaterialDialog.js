import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  Modal,
  Text,
  Platform,
  TouchableHighlight,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  View,
  Dimensions
} from 'react-native';
import colors from './colors';
import { material } from 'react-native-typography';
import {SearchBar} from 'react-native-elements';

const { height } = Dimensions.get('window');

// TODO: Don't rely on Dimensions for the actions footer layout
// TODO: Support custom actions
// TODO: Stacked full-width buttons

const ActionButton = ({ testID, onPress, colorAccent, label }) => {
  return (
    <TouchableHighlight
      testID={testID}
      style={styles.actionContainer}
      underlayColor={colors.androidPressedUnderlay}
      onPress={() => {
        if (onPress) {
          onPress();
        }
      }}
    >
      <Text style={[material.button, {color: colorAccent}]}>{label}</Text>
    </TouchableHighlight>
  );
};

const MaterialDialog = ({
                          visible,
                          scrolled,
                          title,
                          titleColor,
                          colorAccent,
                          backgroundColor,
                          addPadding,
                          onOk,
                          onCancel,
                          okLabel,
                          cancelLabel,
                          children,
                          okOnItemSelect,
                          searchQuery,
                          onSearchTextChange
                        }) => {
  const resetSearchQuery = () => {
    if (onSearchTextChange) {
      onSearchTextChange('');
    }
  };

  return (
    <Modal
      animationType={'fade'}
      transparent
      hardwareAccelerated
      visible={visible}
      onRequestClose={onCancel}
      supportedOrientations={['portrait', 'landscape']}
    >
      <TouchableWithoutFeedback onPress={onCancel}>
        <View style={styles.backgroundOverlay}>
          <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : null} keyboardVerticalOffset={-150}>
            <View
              style={[
                styles.modalContainer,
                (title != null || (addPadding && title == null)) && styles.modalContainerPadding,
                {backgroundColor},
                {minWidth: '85%', maxWidth: 400}
              ]}
            >
              <TouchableWithoutFeedback>
                <View>
                  {title != null ? (
                    <View style={scrolled ? styles.titleContainerScrolled : styles.titleContainer}>
                      <Text style={[material.title, {color: titleColor}]}>{title}</Text>
                    </View>
                  ) : null}
                  <View
                    style={{
                      flexGrow: 1,
                    }}
                  >
                    <View
                      style={{
                        width: '100%',
                        height: onSearchTextChange ? 44 : 0,
                        marginLeft: 0,
                        paddingLeft: 0,
                        overflow: 'hidden'
                      }}
                    >
                      <SearchBar
                        containerStyle={{
                          height: 30,
                          flex: 1,
                          backgroundColor: 'transparent',
                        }}
                        inputContainerStyle={{minHeight: 30, height: 30}}
                        inputStyle={{marginLeft: Platform.OS === 'ios' ? 8 : 0}}
                        platform={'android'}
                        cancelButtonProps={{
                          color: colorAccent,
                          buttonTextStyle: {
                            fontFamily: 'Lato'
                          }
                        }}
                        value={searchQuery}
                        onChangeText={(text) => {
                          if(onSearchTextChange){
                            onSearchTextChange(text);
                          }
                        }}
                      />
                    </View>
                    <View
                      style={
                        scrolled
                          ? [
                            styles.contentContainerScrolled,
                            addPadding && styles.contentContainerScrolledPadding,
                          ]
                          : [styles.contentContainer, addPadding && styles.contentContainerPadding]
                      }
                    >
                      {children}
                    </View>
                  </View>
                  {onOk != null && onCancel != null ? (
                    <View
                      style={scrolled ? styles.actionsContainerScrolled : styles.actionsContainer}
                    >
                      <ActionButton
                        testID="dialog-cancel-button"
                        colorAccent={colorAccent}
                        onPress={() => {
                          resetSearchQuery();
                          if(onCancel){
                            onCancel();
                          }
                        }}
                        label={cancelLabel}
                      />
                      {okOnItemSelect ? null : (
                        <ActionButton
                          testID="dialog-ok-button"
                          colorAccent={colorAccent}
                          onPress={() => {
                            resetSearchQuery();
                            if(onOk){
                              onOk();
                            }
                          }}
                          label={okLabel}
                        />
                      )}
                    </View>
                  ) : null}
                </View>
              </TouchableWithoutFeedback>
            </View>
          </KeyboardAvoidingView>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
};

const styles = StyleSheet.create({
  backgroundOverlay: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.backgroundOverlay
  },
  modalContainer: {
    marginHorizontal: 16,
    marginVertical: 106,
    minWidth: 280,
    borderRadius: 2,
    elevation: 1,
    overflow: 'hidden',
  },
  modalContainerPadding: {
    paddingTop: 24,
  },
  titleContainer: {
    paddingHorizontal: 24,
    paddingBottom: 20,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  titleContainerScrolled: {
    paddingHorizontal: 24,
    paddingBottom: 20,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: colors.androidBorderColor,
  },
  contentContainer: {
    flex: -1,
  },
  contentContainerPadding: {
    paddingHorizontal: 24,
    paddingBottom: 24,
  },
  contentContainerScrolled: {
    flex: -1,
    maxHeight: height - 264, // (106px vertical margin * 2) + 52px
    borderTopWidth: StyleSheet.hairlineWidth,
    borderColor: colors.androidBorderColor,
  },
  contentContainerScrolledPadding: {
    paddingHorizontal: 24,
  },
  actionsContainer: {
    height: 52,
    minHeight: 52,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingLeft: 8,
  },
  actionsContainerScrolled: {
    height: 52,
    minHeight: 52,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingLeft: 8,
    borderTopWidth: StyleSheet.hairlineWidth,
    borderColor: colors.androidBorderColor,
  },
  actionContainer: {
    marginRight: 8,
    minHeight: 52,
    paddingHorizontal: 8,
    paddingVertical: 8,
    minWidth: 64,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

MaterialDialog.propTypes = {
  visible: PropTypes.bool.isRequired,
  children: PropTypes.element.isRequired,
  onCancel: PropTypes.func.isRequired,
  onOk: PropTypes.func,
  cancelLabel: PropTypes.string,
  okLabel: PropTypes.string,
  title: PropTypes.string,
  titleColor: PropTypes.string,
  backgroundColor: PropTypes.string,
  colorAccent: PropTypes.string,
  scrolled: PropTypes.bool,
  addPadding: PropTypes.bool,
  okOnItemSelect: PropTypes.bool,
  searchQuery: PropTypes.string,
  onSearchTextChange: PropTypes.func,
};

MaterialDialog.defaultProps = {
  okLabel: 'OK',
  cancelLabel: 'CANCEL',
  title: undefined,
  titleColor: colors.androidPrimaryTextColor,
  backgroundColor: colors.background,
  colorAccent: colors.androidColorAccent,
  scrolled: false,
  addPadding: true,
  onOk: undefined,
  onCancel: undefined,
  okOnItemSelect: false,
  searchQuery: undefined,
  onSearchTextChange: undefined
};

ActionButton.propTypes = {
  testID: PropTypes.string.isRequired,
  colorAccent: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
};

export default MaterialDialog;
